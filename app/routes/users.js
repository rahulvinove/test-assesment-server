const multer  = require('multer')
const upload = multer({ dest: 'uploads/' })


module.exports = app => {
    const users = require("../controllers/user.controller.js");
    var router = require("express").Router();
    router.post("/", upload.single('export'), users.upload);
    router.get("/", users.findAll);
    app.use('/api/users', router);
  };