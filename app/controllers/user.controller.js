const db = require("../models");
const User = db.users;
const fs = require("fs");
const csv = require("fast-csv");

exports.upload = (req, res) => {
    try {
        if (req.file == undefined) {
          return res.status(400).send("Please upload a CSV file!");
        }
        let users = [];
        let path = __basedir + "/uploads/" + req.file.filename;
        fs.createReadStream(path)
          .pipe(csv.parse({ headers: true }))
          .on("error", (error) => {
            throw error.message;
          })
          .on("data", (row) => {
            users.push(row);
          })
          .on("end", () => {
            
            User.destroy({ truncate : true, cascade: false }).then(()=>{
                User.bulkCreate(users)
                .then(() => {
                    res.status(200).send({
                    message:
                        "Uploaded the file successfully: " + req.file.originalname,
                    });
                })
                .catch((error) => {
                    res.status(500).send({
                    message: "Fail to import data into database!",
                    error: error.message,
                    });
                });
            })
            
          });
      } catch (error) {
        console.log(error);
        res.status(500).send({
          message: "Could not upload the file: " + req.file.originalname,
        });
      }
};

exports.findAll = (req, res) => {
    User.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};